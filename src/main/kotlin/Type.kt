package org.memobase

enum class Type {
    institution,
    record_set
}