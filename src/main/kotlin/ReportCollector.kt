/*
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import com.beust.klaxon.Klaxon
import org.apache.logging.log4j.LogManager
import java.net.HttpURLConnection
import java.net.URL

class ReportCollector(importApiUrl: String, private val env: String) {
    private val log = LogManager.getLogger(this::class.java)
    private val collection = mutableMapOf<String, ProcessReport>()
    private val requiredStepsForInstitutions = 4
    private val requiredStepsForRecordSets = 4
    private val klaxon = Klaxon()

    private val url = URL(importApiUrl)

    fun addReport(identifier: String, type: Type, session: String, report: Report) {
        val key = identifier + session
        if (collection.containsKey(key)) {
            val value = collection[key]!!
            collection[key] = ProcessReport(
                identifier,
                type,
                report.status == ReportStatus.fatal,
                value.stepsCompleted + 1,
                "${value.message}\n${report.message}"
            )
        } else {
            collection[identifier + session] = ProcessReport(
                identifier,
                type,
                report.status == ReportStatus.fatal,
                1,
                report.message
            )
        }
    }

    fun check() {
        val removedKeys = collection
            .filter { it.value.fatal || hasRequired(it.value.stepsCompleted, it.value.type) }
            .map {
                log.info("Sending report for report ${it.value.identifier}.")
                Pair(it.key, it.value.toRequestBody().toJson(klaxon))
            }
            .map {
                val con = url.openConnection()
                val http = con as HttpURLConnection
                http.requestMethod = "POST"
                http.doOutput = true
                http.setFixedLengthStreamingMode(it.second.size)
                http.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                http.setRequestProperty("User-Agent", "gi-reports-consumer-$env")
                http.connect()
                http.outputStream.use { os -> os.write(it.second) }
                if (http.responseCode >= 400) {
                    http.errorStream.use { errorStream ->
                        log.error(errorStream.bufferedReader().lines().reduce { s: String?, s2: String? -> s + s2 })
                    }
                }
                http.disconnect()
                it.first
            }

        for (key in removedKeys)
            collection.remove(key)
    }

    private fun hasRequired(completed: Int, type: Type): Boolean {
        return when (type) {
            Type.institution -> completed >= requiredStepsForInstitutions
            Type.record_set -> completed >= requiredStepsForRecordSets
        }
    }
}