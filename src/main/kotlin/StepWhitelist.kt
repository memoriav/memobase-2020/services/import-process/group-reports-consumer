package org.memobase

import org.apache.logging.log4j.LogManager
import java.io.File
import java.nio.charset.Charset

class StepWhitelist(path: String) {
    private val log = LogManager.getLogger(this::class.java)
    private val whitelist = File(path).readLines(Charset.defaultCharset())

    fun check(step: String): Boolean {
        return if (whitelist.contains(step)) {
            log.info("Step $step is present in whitelist. Processing report.")
            true
        } else {
            log.info("Step $step is not present in whitelist. Not processing.")
            false
        }
    }

    fun str(): String {
        return whitelist.joinToString(", ")
    }
}