/*
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import ch.memobase.reporting.Report
import java.io.Closeable
import java.util.Properties
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.Headers

class Producer(
    private val reportingTopic: String,
    producerProps: Properties
) : Closeable {
    private val instance = KafkaProducer<String, String>(producerProps)
    fun sendReport(report: Report, headers: Headers?) {
        instance.send(ProducerRecord(reportingTopic, null, report.id, report.toJson(), headers))
    }
    override fun close() {
        instance.close()
    }
}
