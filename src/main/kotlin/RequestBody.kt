/*
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import com.beust.klaxon.Klaxon
import org.apache.logging.log4j.LogManager

data class RequestBody(
    val node_type: String,
    val id: String,
    val status: Boolean,
    val report: String
) {
    private val log = LogManager.getLogger(this::class.java)

    fun toJson(klaxon: Klaxon): ByteArray {
        val message = klaxon.toJsonString(this)
        log.debug(message)
        return message.toByteArray()
    }
}
