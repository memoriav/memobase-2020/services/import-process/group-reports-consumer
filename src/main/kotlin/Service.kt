/*
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import ch.memobase.settings.SettingsLoader
import com.beust.klaxon.Klaxon
import org.apache.logging.log4j.LogManager

class Service(file: String = "app.yml") {
    companion object {
        const val importApiUrlSettingName = "importApiUrl"
        const val whitelistFilePathSettingName = "whitelistFilePath"
        const val envPropName = "env"
    }

    private val log = LogManager.getLogger("GroupReportsConsumerService")

    private val settings = SettingsLoader(
        listOf(
            importApiUrlSettingName,
            whitelistFilePathSettingName,
            envPropName
        ),
        file,
        useConsumerConfig = true,
        useProducerConfig = true
    )

    private val klaxon = Klaxon()
    private val consumer = Consumer(settings.inputTopic, settings.kafkaConsumerSettings)
    private val producer = Producer(settings.outputTopic, settings.kafkaProducerSettings)
    private val importApiUrl = settings.appSettings[importApiUrlSettingName] as String
    private val stepWhitelist = StepWhitelist(settings.appSettings[whitelistFilePathSettingName] as String)

    private val collector = ReportCollector(importApiUrl, settings.appSettings.getProperty(envPropName))

    fun run() {
        log.info("Begin reading from topic ${settings.inputTopic} on kafka cluster.")
        log.info("Sending aggregated reports to ${importApiUrl}.")
        log.info("Reading the following steps: ${stepWhitelist.str()}")
        while (true) {
            consumer.consume()
                .mapNotNull { klaxon.parse<IndexReport>(it.value()) }
                .filter { stepWhitelist.check(it.report.step) }
                .forEach {
                    log.info("Processing report: " + it.report.id + "; " + it.report.step)
                    if (it.metadata.institutionId == "none") {
                        collector.addReport(
                            it.metadata.recordSetId,
                            Type.record_set,
                            it.metadata.sessionId,
                            it.report
                        )
                    } else {
                        collector.addReport(
                            it.metadata.institutionId,
                            Type.institution,
                            it.metadata.sessionId,
                            it.report
                        )
                    }

                }
            collector.check()
        }
    }
}

