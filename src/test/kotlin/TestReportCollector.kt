/*
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.memobase.ReportCollector
import org.memobase.Type

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestReportCollector {
    private val stageURL = "https://test.import.memobase.k8s.unibas.ch/v1/drupal/WriteElementReport"

    @Test
    fun `test collection`() {
        val collector = ReportCollector(stageURL, "test")

        collector.addReport(
            "csa",
            Type.institution,
            "1",
            Report(
                "csa",
                ReportStatus.fatal,
                "Test message new 1",
                "gi-drupal-syncer"
            )
        )

        collector.check()
    }
}